-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2022 at 03:47 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `id` int(11) NOT NULL,
  `periode` date NOT NULL,
  `jumlah_hari` int(11) NOT NULL,
  `lembur` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`id`, `periode`, `jumlah_hari`, `lembur`) VALUES
(6, '2022-06-20', 26, 7500),
(7, '2022-07-01', 25, 7500);

-- --------------------------------------------------------

--
-- Table structure for table `ijin`
--

CREATE TABLE `ijin` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `jumlah_hari` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis` enum('ijin','cuti') NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('diterima','ditolak','diproses') NOT NULL,
  `status_keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ijin`
--

INSERT INTO `ijin` (`id`, `pegawai`, `jumlah_hari`, `keterangan`, `jenis`, `tanggal`, `status`, `status_keterangan`) VALUES
(3, 45, 4, 'Sakit', 'cuti', '2022-05-23', 'diterima', ''),
(4, 45, 3, 'Sakit', 'ijin', '2022-05-24', 'ditolak', ''),
(5, 45, 3, 'Sakit', 'ijin', '2022-05-23', 'ditolak', ''),
(6, 45, 3, 'as', 'ijin', '2022-05-22', 'ditolak', 'Terlalu Lama'),
(7, 45, 2, 'Sakit', 'cuti', '2022-05-24', 'diterima', ''),
(8, 48, 1, 'acara', 'ijin', '2022-06-19', 'diterima', '');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `nama`) VALUES
(2, 'Staff Gudang'),
(3, 'Staff Akuntings'),
(4, 'Manager Produksi'),
(7, 'Operatos Desain Jersey');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nomor_telepon` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` int(11) NOT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `gaji_pokok` varchar(50) NOT NULL,
  `tunjangan` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`user`, `nama`, `tanggal_lahir`, `nik`, `nomor_telepon`, `alamat`, `jabatan`, `tanggal_masuk`, `gaji_pokok`, `tunjangan`, `foto`) VALUES
(44, 'Bambang Pamungkasa', '2022-04-29', '37.64.78.93.64.36-2343', '6285 1562 3006', '<p>asasa</p>', 4, '2022-05-17', '5,000,000', '1,500,000', ''),
(45, 'Rifki Alfaridzi', '1998-04-30', '37.64.78.93.00.36-2343', '6285 1562 3006', '<p>asas</p>', 4, '2021-04-30', '5,000,000', '1,500,000', 'glHzOG8Dfr.jpg'),
(48, 'Paulo', '2022-05-17', '37.64.78.93.00.36-2343', '6285 1562 3006', '<p>asas</p>', 3, '2022-05-15', '2,000,000', '1,500,000', 'LKxilnW3kj.jpg'),
(50, 'Mandala Septu', '2002-07-07', '33.72.07.67.36.28-2837', '0896 5434 5768', '<p>Karanganyar</p>', 7, '2021-06-26', '3,500,000', '0', 'dummy.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `record_absen`
--

CREATE TABLE `record_absen` (
  `id` int(11) NOT NULL,
  `shift` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `waktu` time DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `gap` float NOT NULL,
  `jenis` enum('absen masuk','absen pulang','lembur masuk','lembur pulang','cuti') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `record_absen`
--

INSERT INTO `record_absen` (`id`, `shift`, `pegawai`, `waktu`, `tanggal`, `gap`, `jenis`) VALUES
(5, 10, 45, '07:21:00', '2022-05-17', 1, 'absen masuk'),
(8, 10, 45, '07:21:00', '2022-05-18', 0, 'absen masuk'),
(9, 10, 45, '20:51:00', '2022-05-19', 14, 'absen masuk'),
(11, 10, 45, '20:55:00', '2022-05-19', 0, 'absen pulang'),
(12, 10, 45, '10:02:00', '2022-05-22', 4, 'absen masuk'),
(16, 10, 45, '10:41:00', '2022-05-22', 0, 'absen pulang'),
(17, 10, 45, '08:42:00', '2022-05-22', 0, 'lembur masuk'),
(21, 10, 45, '11:01:00', '2022-05-22', 2, 'lembur pulang'),
(22, 10, 45, '11:01:00', '2022-05-22', 3, 'lembur pulang'),
(23, 10, 45, '10:02:00', '2022-06-05', 4, 'absen masuk'),
(24, 10, 48, '10:48:00', '2022-06-19', 4, 'absen masuk'),
(25, 10, 48, '10:48:00', '2022-06-19', 0, 'absen pulang'),
(26, 10, 48, '10:49:00', '2022-06-19', 0, 'lembur masuk'),
(27, 10, 48, '10:49:00', '2022-06-19', 0, 'lembur pulang'),
(28, 10, 50, '11:14:00', '2022-06-26', 5, 'absen masuk');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_pulang` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `nama`, `jam_masuk`, `jam_pulang`) VALUES
(10, 'Shift Pagi', '07:00:00', '14:00:00'),
(11, 'Shift Siang', '12:00:00', '21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `created_at`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2020-03-21'),
(45, 'rifkialfa', '21232f297a57a5a743894a0e4a801fc3', 3, '2022-04-30'),
(48, 'paulo', '21232f297a57a5a743894a0e4a801fc3', 3, '2022-05-15'),
(49, 'pimpinan', '21232f297a57a5a743894a0e4a801fc3', 1, '2020-03-21'),
(50, 'mandalaseptu', 'cfba52bd2f1558fca5b4705ee10ffeda', 3, '2022-06-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ijin`
--
ALTER TABLE `ijin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai` (`pegawai`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD UNIQUE KEY `user` (`user`) USING BTREE,
  ADD KEY `jabatan` (`jabatan`);

--
-- Indexes for table `record_absen`
--
ALTER TABLE `record_absen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai` (`pegawai`),
  ADD KEY `shift` (`shift`) USING BTREE;

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ijin`
--
ALTER TABLE `ijin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `record_absen`
--
ALTER TABLE `record_absen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
