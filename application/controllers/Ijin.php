<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Ijin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Ijin_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main_data()
	{
		header('Content-Type: application/json');
		$Ijin =  $this->Ijin_model->main_data();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Ijin == null ? [] : count($Ijin);
		$data['recordsFiltered'] = $Ijin == null ? [] : count($Ijin);
		$data['data'] = $Ijin == null ? [] : $Ijin;

		echo json_encode($data);
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'ijin/main';
		$data['page_title'] = 'Halaman Detail Jabatan';
		$this->load->view('template', $data);
	}

	// public function update_status($type, $id)
	// {
	// 	$this->Ijin_model->update_status($id, ["status" => $type]);
	// 	$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
	// 	redirect(site_url('ijin/main'));
	// }

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] == 1 && $data_session['level'] == 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'ijin/ijin';
		$data['page_title'] = 'Halaman Detail Perijinan';
		$this->load->view('template', $data);
	}

	public function print_ijin()
	{
			$data_ijin = $this->Ijin_model->main_data();
			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "laporan_ijin.pdf";
			$this->pdf->load_view('laporan/laporan_ijin_all', ['data_ijin' => $data_ijin]);
	}

	public function create()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] == 1 && $data_session['level'] == 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'ijin/create';
		$data['page_title'] = 'Halaman Perijinan';
		$this->load->view('template', $data);
	}


	public function json()
	{
		header('Content-Type: application/json');
		$data_session = $this->session->userdata;
		$Ijin =  $this->Ijin_model->json($data_session['id']);

		$data['draw'] = 0;
		$data['recordsTotal'] = $Ijin == null ? [] : count($Ijin);
		$data['recordsFiltered'] = $Ijin == null ? [] : count($Ijin);
		$data['data'] = $Ijin == null ? [] : $Ijin;

		echo json_encode($data);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$data_session = $this->session->userdata;
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('ijin'));
			//echo validation_errors();
		} else {
			date_default_timezone_set('Asia/Jakarta');
			$data = $this->input->post();
			$data['pegawai'] = $data_session['id'];
			$data['tanggal'] = date('Y-m-d');
			$data['status'] = "diproses";

			// Insert Data Default Teknik Jabatan
			$this->Ijin_model->insert($data);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('ijin'));
		}
	}

	public function detail($id)
	{
		
		$row = $this->Ijin_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_ijin' => $row,
				'main_content' => 'ijin/detail',
				'page_title' => 'Detail Ijin'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('ijin/main'));
		}
	}


	public function edit($id)
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] == 1 && $data_session['level'] == 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$row = $this->Ijin_model->get_by_id($id);

		if ($row) {
			if ($row->status != "diterima") {
				$data = array(
					'data_ijin' => $row,
					'main_content' => 'ijin/update',
					'page_title' => 'Edit Ijin'
				);
				$this->load->view('template', $data);
			} else {
				$this->session->set_flashdata('pesan', 'Data Tidak Bisa Diubah');
				redirect(site_url('ijin'));
			}
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('ijin'));
		}
	}



	public function update_action($id)
	{

		$jabatan = $this->Ijin_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('ijin'));
		} else {

			if (empty($jabatan)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('ijin'));
			}

			$data_ijin = $this->input->post();
			$data_ijin['status'] = "diproses";
			$data_ijin['status_keterangan'] = "";
			$this->Ijin_model->update($id, $data_ijin);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('ijin'));
		}
	}

	public function update_status($id)
	{
		//var_dump($this->input->post());

		$jabatan = $this->Ijin_model->get_by_id($id);

		
		if (empty($jabatan)) {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('ijin/main'));
		}


		$this->Ijin_model->update($id, $this->input->post());
		$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
		redirect(site_url('ijin/main'));
	}

	public function delete($id)
	{
		$row = $this->Ijin_model->get_by_id($id);

		if ($row) {
			if ($row->status != "diterima") {
				$this->Ijin_model->delete($id);
				$this->session->set_flashdata('pesan', 'Data Berhasil Dihapus');
				redirect(site_url('ijin'));
			} else {
				$this->session->set_flashdata('pesan', 'Data Tidak Bisa Dihapus');
				redirect(site_url('ijin'));
			}
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('ijin'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('jenis', 'Keperluan', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('jumlah_hari', 'Jumlah Hari', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
