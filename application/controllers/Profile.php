<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 3) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		// ADDONS
		$this->load->model('Jabatan_model');
		$this->load->model('Profile_model');
		$this->load->model('Shift_model');
		$this->load->model('Pegawai_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	// public function main()
	// {

	// 	$data_session = $this->session->userdata;
	// 	if ((!$this->session->userdata('logged_in'))) {
	// 		redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
	// 	}

	// 	$data['dataPegawai'] = $this->Pegawai_model->get_by_idPegawai($data_session['id'])->result();

	// 	if (empty($data['dataPegawai'])) {
	// 		$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
	// 		redirect(site_url('lineup'));
	// 	} else {
	// 		$data['main_content'] = 'Pegawai/detail';
	// 		$data['page_title'] = 'Halaman Detail Pegawai';
	// 		$this->load->view('template', $data);
	// 	}
	// }

	public function index()
	{
		$data_session = $this->session->userdata;
		$data['recent_absen'] = $this->Profile_model->all_record_absen_by_pegawai($data_session['id']);
		
		$data['main_content'] = 'profile/main';
		$data['page_title'] = 'Selamat Datang';
		$this->load->view('template', $data);
	}

	public function ijin()
	{
		$data_session = $this->session->userdata;
		$data['main_content'] = 'ijin/main';
		$data['page_title'] = 'Selamat Datang';
		$this->load->view('template', $data);
	}

	public function data_absen($type = null, $month = null)
	{
		$data_session = $this->session->userdata;

		switch ($type) {
			case "masuk":
				header('Content-Type: application/json');
				
				if (is_null($month)) {
					$data['draw'] = 0;
					$data['recordsTotal'] = 0;
					$data['recordsFiltered'] = 0;
					$data['data'] = [];
				}else{
					$data_absen_masuk = $this->Profile_model->record_absen_by_user($data_session['id'], "absen masuk", $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['recordsFiltered'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['data'] = $data_absen_masuk == null ? [] : $data_absen_masuk;
				}
				echo json_encode($data);

				break;
			case "pulang":
				header('Content-Type: application/json');
				
				if (is_null($month)) {
					$data_absen_masuk = $this->Profile_model->record_absen_by_user($data_session['id'], "absen pulang", $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = 0;
					$data['recordsFiltered'] = 0;
					$data['data'] = [];
				}else{
					$data_absen_masuk = $this->Profile_model->record_absen_by_user($data_session['id'], "absen pulang", $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['recordsFiltered'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['data'] = $data_absen_masuk == null ? [] : $data_absen_masuk;
				}
				echo json_encode($data);

				break;
			default:
				echo "kesalahan";
		}
	}

	public function absen($type = null)
	{
		$data_session = $this->session->userdata;
		switch ($type) {
			case "masuk":

				$data['main_content'] = 'profile/absen/masuk';
				$data['page_title'] = 'Selamat Datang Silahkan Melakukan Absen Masuk';
				$data['absen_masuk'] = $this->Profile_model->record_absen($data_session['id'], "absen masuk");
				$this->load->view('template', $data);
				break;
			case "pulang":

				$data['main_content'] = 'profile/absen/pulang';
				$data['page_title'] = 'Selamat Datang Silahkan Melakukan Absen Pulang';
				$data['absen_pulang'] = $this->Profile_model->record_absen($data_session['id'], "absen pulang");
				$this->load->view('template', $data);

				break;
			default:
				echo "kesalahan";
		}
	}

	public function do_absen($type = null, $sift = null)
	{
		switch ($type) {
			case "masuk":

				$data_session = $this->session->userdata;
				if (is_null($sift)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
					redirect(site_url('profile/absen/masuk'));
				} else {
					$data_sift = base64_decode($sift);

					$unserialize_sift = unserialize($data_sift);

					$row = $this->Shift_model->get_by_id($unserialize_sift["kode"]);
					if ($row) {
						date_default_timezone_set('Asia/Jakarta');
						$row_date_absen = strtotime(date("Y-m-d ") . $row->jam_masuk); // Jam Sesuai Master Database
						$row_date_record = strtotime(date("Y-m-d H:i:s")); // Jam Absen Realtime/Sekarang

						$diff = $row_date_record - $row_date_absen;
						$selisih_jam = $diff / (60 * 60);

						$data_absen['shift'] =  $unserialize_sift["kode"];
						$data_absen['pegawai'] =  $data_session['id'];
						$data_absen['waktu'] =  date('H:i');
						$data_absen['tanggal'] =  date('Y-m-d');
						$data_absen['gap'] =  ceil($selisih_jam);
						$data_absen['jenis'] =  "absen masuk";

						$validasi_data = $this->Profile_model->record_absen_validation($data_absen); // CHECKDOUBLE ABSEN

						if ($validasi_data) {
							$this->session->set_flashdata('pesan', 'Anda Sudah Melakukan Absen Masuk');
							redirect(site_url('profile/absen/masuk'));
						} else {
							$this->Profile_model->insert_absen($data_absen);
							$this->session->set_flashdata('pesan', 'Absen Sukses :' . $data_absen['tanggal'] . " " . $data_absen['waktu']);
							redirect(site_url('profile/absen/masuk'));
						}
					} else {
						$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
						redirect(site_url('profile/absen/masuk'));
					}
				}

				// $data_session = $this->session->userdata;
				// var_dump($data_session['id']);
				// $data_absen['pegawai'] = $unserialize_sift["kode"];

				break;
			case "pulang":


				$data_session = $this->session->userdata;
				if (is_null($sift)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
					redirect(site_url('profile/absen/pulang'));
				} else {
					$data_sift = base64_decode($sift);

					$unserialize_sift = unserialize($data_sift);

					$row = $this->Shift_model->get_by_id($unserialize_sift["kode"]);
					if ($row) {
						date_default_timezone_set('Asia/Jakarta');

						$data_absen['shift'] =  $unserialize_sift["kode"];
						$data_absen['pegawai'] =  $data_session['id'];
						$data_absen['waktu'] =  date('H:i');
						$data_absen['tanggal'] =  date('Y-m-d');
						$data_absen['gap'] =  0;
						$data_absen['jenis'] =  "absen pulang";

						$data_absen_masuk = ['pegawai' => $data_session['id'], 'jenis' => 'absen masuk'];
						$validasi_absen_masuk = $this->Profile_model->record_absen_validation($data_absen_masuk);
						$validasi_data = $this->Profile_model->record_absen_validation($data_absen); // CHECKDOUBLE ABSEN

						if ($validasi_absen_masuk) {
							if ($validasi_data) {
								$this->session->set_flashdata('pesan', 'Anda Sudah Melakukan Absen Pulang');
								redirect(site_url('profile/absen/pulang'));
							} else {
								$this->Profile_model->insert_absen($data_absen);
								$this->session->set_flashdata('pesan', 'Absen Sukses :' . $data_absen['tanggal'] . " " . $data_absen['waktu']);
								redirect(site_url('profile/absen/pulang'));
							}
						} else {
							$this->session->set_flashdata('pesan', 'Anda Belum Melakukan Absen Masuk');
							redirect(site_url('profile/absen/pulang'));
						}
					} else {
						$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
						redirect(site_url('profile/absen/pulang'));
					}
				}

				break;
			default:
				echo "kesalahan";
		}
	}


	public function data_lembur($type = null, $month = null)
	{
		$data_session = $this->session->userdata;

		switch ($type) {
			case "masuk":
				header('Content-Type: application/json');
				
				if (is_null($month)) {
					$data['draw'] = 0;
					$data['recordsTotal'] = 0;
					$data['recordsFiltered'] = 0;
					$data['data'] = [];
				}else{
					$data_lembur_masuk = $this->Profile_model->record_absen_by_user($data_session['id'], "lembur masuk", $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = $data_lembur_masuk == null ? [] : count($data_lembur_masuk);
					$data['recordsFiltered'] = $data_lembur_masuk == null ? [] : count($data_lembur_masuk);
					$data['data'] = $data_lembur_masuk == null ? [] : $data_lembur_masuk;
				}
				echo json_encode($data);

				break;
			case "pulang":
				header('Content-Type: application/json');
				
				if (is_null($month)) {
					$data['draw'] = 0;
					$data['recordsTotal'] = 0;
					$data['recordsFiltered'] = 0;
					$data['data'] = [];
				}else{
					$data_absen_masuk = $this->Profile_model->record_absen_by_user($data_session['id'], "lembur pulang", $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['recordsFiltered'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['data'] = $data_absen_masuk == null ? [] : $data_absen_masuk;
				}
				echo json_encode($data);

				break;
			default:
				echo "kesalahan";
		}
	}

	public function lembur($type = null)
	{
		switch ($type) {
			case "masuk":

				$data['main_content'] = 'profile/lembur/masuk';
				$data['page_title'] = 'Selamat Datang Silahkan Melakukan Lembur Masuk';
				$data['lembur_masuk'] = [];
				$this->load->view('template', $data);

				break;
			case "pulang":

				$data['main_content'] = 'profile/lembur/pulang';
				$data['page_title'] = 'Selamat Datang Silahkan Melakukan Lembur Pulang';
				$data['lembur_pulang'] = [];
				$this->load->view('template', $data);

				break;
			default:
				echo "kesalahan";
		}
	}

	public function do_lembur($type = null, $sift = null)
	{
		switch ($type) {
			case "masuk":

				$data_session = $this->session->userdata;
				if (is_null($sift)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
					redirect(site_url('profile/lembur/masuk'));
				} else {
					$data_sift = base64_decode($sift);

					$unserialize_sift = unserialize($data_sift);

					$row = $this->Shift_model->get_by_id($unserialize_sift["kode"]);
					if ($row) {
						date_default_timezone_set('Asia/Jakarta');

						// $row_date_lembur = strtotime(date("Y-m-d ").$row->jam_masuk); // Jam Sesuai Master Database
						// $row_date_record = strtotime(date("Y-m-d H:i:s")); // Jam lembur Realtime/Sekarang

						// $diff = $row_date_record - $row_date_lembur;
						// $selisih_jam = $diff / (60*60);

						$data_lembur['shift'] =  $unserialize_sift["kode"];
						$data_lembur['pegawai'] =  $data_session['id'];
						$data_lembur['waktu'] =  date('H:i');
						$data_lembur['tanggal'] =  date('Y-m-d');
						$data_lembur['gap'] =  0;
						$data_lembur['jenis'] =  "lembur masuk";

						$validasi_absen_masuk = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "jenis" => "absen masuk"]);
						$validasi_absen_pulang = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "jenis" => "absen pulang"]);

						if ($validasi_absen_masuk && $validasi_absen_pulang) {
							$validasi_data = $this->Profile_model->record_lembur_validation($data_lembur); // CHECKDOUBLE lembur

							if ($validasi_data) {
								$this->session->set_flashdata('pesan', 'Anda Sudah Melakukan lembur Masuk');
								redirect(site_url('profile/lembur/masuk'));
							} else {
								$this->Profile_model->insert_lembur($data_lembur);
								$this->session->set_flashdata('pesan', 'lembur Sukses :' . $data_lembur['tanggal'] . " " . $data_lembur['waktu']);
								redirect(site_url('profile/lembur/masuk'));
							}
						} else {
							$this->session->set_flashdata('pesan', 'Anda Belum Menyelesaikan Proses Absen');
							redirect(site_url('profile/lembur/masuk'));
						}
					} else {
						$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
						redirect(site_url('profile/lembur/masuk'));
					}
				}

				// $data_session = $this->session->userdata;
				// var_dump($data_session['id']);
				// $data_lembur['pegawai'] = $unserialize_sift["kode"];

				break;
			case "pulang":


				$data_session = $this->session->userdata;
				if (is_null($sift)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
					redirect(site_url('profile/lembur/pulang'));
				} else {
					$data_sift = base64_decode($sift);

					$unserialize_sift = unserialize($data_sift);

					$row = $this->Shift_model->get_by_id($unserialize_sift["kode"]);
					if ($row) {
						date_default_timezone_set('Asia/Jakarta');

						$data_lembur['shift'] =  $unserialize_sift["kode"];
						$data_lembur['pegawai'] =  $data_session['id'];
						$data_lembur['waktu'] =  date('H:i');
						$data_lembur['tanggal'] =  date('Y-m-d');
						$data_lembur['jenis'] =  "lembur pulang";

						$data_lembur_masuk = ['pegawai' => $data_session['id'], 'jenis' => 'lembur masuk'];
						$validasi_lembur_masuk = $this->Profile_model->record_lembur_validation($data_lembur_masuk);
						$validasi_data = $this->Profile_model->record_lembur_validation($data_lembur); // CHECKDOUBLE lembur

						$validasi_absen_masuk = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "jenis" => "absen masuk"]);
						$validasi_absen_pulang = $this->Profile_model->record_absen_validation(["pegawai" => $data_session['id'], "jenis" => "absen pulang"]);

						if ($validasi_absen_masuk && $validasi_absen_pulang) {
							$validasi_data = $this->Profile_model->record_lembur_validation($data_lembur); // CHECKDOUBLE lembur

							if ($validasi_data) {
								$this->session->set_flashdata('pesan', 'Anda Sudah Melakukan lembur Masuk');
								redirect(site_url('profile/lembur/pulang'));
							} else {

								if ($validasi_lembur_masuk) {

									if ($validasi_data) {
										$this->session->set_flashdata('pesan', 'Anda Sudah Melakukan lembur Pulang');
										redirect(site_url('profile/lembur/pulang'));
									} else {

										$lembur_masuk = strtotime(date("Y-m-d ") . $validasi_lembur_masuk->waktu);
										$lembur_pulang = strtotime(date("Y-m-d H:i:s"));
										$diff = $lembur_pulang - $lembur_masuk;
										$selisih_jam = floor($diff / (60 * 60));
										$data_lembur['gap'] =  $selisih_jam;

										$this->Profile_model->insert_lembur($data_lembur);
										$this->session->set_flashdata('pesan', 'lembur Sukses :' . $data_lembur['tanggal'] . " " . $data_lembur['waktu']);
										redirect(site_url('profile/lembur/pulang'));
									}
								} else {
									$this->session->set_flashdata('pesan', 'Anda Belum Melakukan lembur Masuk');
									redirect(site_url('profile/lembur/pulang'));
								}
							}
						} else {
							$this->session->set_flashdata('pesan', 'Anda Belum Menyelesaikan Proses Absen');
							redirect(site_url('profile/lembur/pulang'));
						}
					} else {
						$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
						redirect(site_url('profile/lembur/pulang'));
					}
				}

				break;
			default:
				echo "kesalahan";
		}
	}

	// public function create()
	// {
	// 	$data['jabatan'] = $this->Jabatan_model->get_all();
	// 	$data['main_content'] = 'Pegawai/create';
	// 	$data['page_title'] = 'Halaman Pegawai';
	// 	$this->load->view('template', $data);
	// }

	// public function json()
	// {
	// 	header('Content-Type: application/json');
	// 	$Pegawai =  $this->Pegawai_model->json();

	// 	$data['draw'] = 0;
	// 	$data['recordsTotal'] = $Pegawai == null ? [] : count($Pegawai);
	// 	$data['recordsFiltered'] = $Pegawai == null ? [] : count($Pegawai);
	// 	$data['data'] = $Pegawai == null ? [] : $Pegawai;

	// 	echo json_encode($data);
	// }
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
