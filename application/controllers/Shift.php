<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Shift extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;


		$this->load->model('Shift_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['dataShift'] = $this->Shift_model->get_by_idShift($data_session['id'])->result();

		if (empty($data['dataShift'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		} else {
			$data['main_content'] = 'shift/detail';
			$data['page_title'] = 'Halaman Detail Shift';
			$this->load->view('template', $data);
		}
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'shift/main';
		$data['page_title'] = 'Halaman Shift';
		$this->load->view('template', $data);
	}

	public function qr($id = null)
	{
		$row = $this->Shift_model->get_by_id($id);

		if ($row) {
			$this->load->library('pdf');
			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "print_produk-";
			$this->pdf->load_view('laporan/qrcode_absen', ['img' => 'assets/img/' . $row->id . '.png', 'data_absen' => $row]);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('master/shift'));
		}
	}

	public function create()
	{

		$data['main_content'] = 'shift/create';
		$data['page_title'] = 'Halaman Shift';
		$this->load->view('template', $data);
	}

	public function coba()
	{
		$data_absen['kode'] = 4302;
		$serialize_absen = serialize($data_absen);
		echo $data = base64_encode($serialize_absen);
		//echo $unserialize = base64_decode($data);
		//var_dump(unserialize($unserialize));
	}


	public function json()
	{
		header('Content-Type: application/json');
		$Shift =  $this->Shift_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Shift == null ? [] : count($Shift);
		$data['recordsFiltered'] = $Shift == null ? [] : count($Shift);
		$data['data'] = $Shift == null ? [] : $Shift;

		echo json_encode($data);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('master/shift'));
			//echo validation_errors();
		} else {

			// Insert Data Default Teknik Shift
			$lastid = $this->Shift_model->insert($this->input->post());
			$this->load->library('ciqrcode');

			$data_absen['kode'] = $lastid;
			$serialize_absen = serialize($data_absen);
			$encode_kode_absen = base64_encode($serialize_absen);


			$params['data'] = $encode_kode_absen;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . '/assets/img/' . $lastid . '.png';
			$this->ciqrcode->generate($params);

			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('master/shift'));
		}
	}

	public function edit($id)
	{

		$row = $this->Shift_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_Shift' => $row,
				'main_content' => 'shift/update',
				'page_title' => 'Edit Shift'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
			redirect(site_url('master/shift'));
		}
	}
	public function update_action($id)
	{

		$Shift = $this->Shift_model->get_by_id($id);

		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
			redirect(site_url('master/shift'));
		} else {
			if (empty($Shift)) {
				$this->session->set_flashdata('pesan', 'Data Tidak Di Temukan');
				redirect(site_url('master/shift'));
			}
			$this->Shift_model->update($id, $this->input->post());

			$this->load->library('ciqrcode');
			
			$data_absen['kode'] = $id;
			$serialize_absen = serialize($data_absen);
			$encode_kode_absen = base64_encode($serialize_absen);

			$params['data'] = $encode_kode_absen;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH . '/assets/img/' . $id . '.png';
			$this->ciqrcode->generate($params);

			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('master/shift'));
		}
	}

	public function delete($id)
	{
		$row = $this->Shift_model->get_by_id($id);

		if ($row) {
			$this->Shift_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('shift'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('shift'));
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
