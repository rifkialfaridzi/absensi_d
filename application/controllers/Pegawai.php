<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		// ADDONS
		$this->load->model('Jabatan_model');

		$this->load->model('Pegawai_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main()
	{

		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['dataPegawai'] = $this->Pegawai_model->get_by_idPegawai($data_session['id'])->result();

		if (empty($data['dataPegawai'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		} else {
			$data['main_content'] = 'pegawai/detail';
			$data['page_title'] = 'Halaman Detail Pegawai';
			$this->load->view('template', $data);
		}
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'pegawai/main';
		$data['page_title'] = 'Halaman Pegawai';
		$this->load->view('template', $data);
	}

	public function data_absen($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
				if (is_null($month)) {
					$data['draw'] = 0;
					$data['recordsTotal'] = 0;
					$data['recordsFiltered'] = 0;
					$data['data'] = [];
				}else{
					$data_absen_masuk = $this->Pegawai_model->record_absen_by_user($id, $month);
					$data['draw'] = 0;
					$data['recordsTotal'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['recordsFiltered'] = $data_absen_masuk == null ? [] : count($data_absen_masuk);
					$data['data'] = $data_absen_masuk == null ? [] : $data_absen_masuk;
				}
				echo json_encode($data);
	}

	public function data_ijin($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
		if (is_null($month)) {
			$data['draw'] = 0;
			$data['recordsTotal'] = 0;
			$data['recordsFiltered'] = 0;
			$data['data'] = [];
		}else{
			$data_ijin = $this->Pegawai_model->record_ijin_by_user($id, $month);
			$data['draw'] = 0;
			$data['recordsTotal'] = $data_ijin == null ? [] : count($data_ijin);
			$data['recordsFiltered'] = $data_ijin == null ? [] : count($data_ijin);
			$data['data'] = $data_ijin == null ? [] : $data_ijin;
		}
		echo json_encode($data);
	}

	public function print_ijin($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
		if (is_null($month)) {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pegawai'));
		}else{
			$data_ijin = $this->Pegawai_model->record_ijin_by_user($id, $month);
			$data_pegawai = $this->Pegawai_model->get_by_id($id);
			$data['pegawai'] = $data_pegawai;
			$data['data_ijin'] = $data_ijin;
		
			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "Laporan Absensi Pegawai-" . $data_pegawai[0]->nama . "-" . $data_pegawai[0]->jabatan . ".pdf";
			$this->pdf->load_view('laporan/laporan_ijin', $data);
		}
		//echo json_encode($data);
	}

	public function print_absen($id = null, $month = null)
	{
		header('Content-Type: application/json');
				
				if (is_null($month) || is_null($month)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Ditemkan');
					redirect(site_url('pegawai'));
				}else{
					$data_pegawai = $this->Pegawai_model->get_by_id($id);
					$data_absen_masuk = $this->Pegawai_model->record_absen_by_user($id, $month);

					if (is_null($data_absen_masuk)) {
						$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
						redirect(site_url('pegawai'));
					}else{

						$data['pegawai'] = $data_pegawai;
						$data['data_absen_masuk'] = $data_absen_masuk;
						$data['periode'] = $month;
				
						$this->load->library('pdf');

						$this->pdf->setPaper('A4', 'potrait');
						$this->pdf->set_option('isRemoteEnabled', TRUE);
						$this->pdf->filename = "Laporan Absensi Pegawai-" . $data_pegawai[0]->nama . "-" . $data_pegawai[0]->jabatan . ".pdf";
						$this->pdf->load_view('laporan/laporan_absensi', $data);

					}
					
					
				}
	}

	public function detail($id)
	{
		$data['id_user'] = $id;
		$data['main_content'] = 'pegawai/detail';
		$data['page_title'] = 'Halaman Detail Pegawai';
		$this->load->view('template', $data);
	}

	public function create()
	{
		$data['jabatan'] = $this->Jabatan_model->get_all();
		$data['main_content'] = 'pegawai/create';
		$data['page_title'] = 'Halaman Pegawai';
		$this->load->view('template', $data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$Pegawai =  $this->Pegawai_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Pegawai == null ? [] : count($Pegawai);
		$data['recordsFiltered'] = $Pegawai == null ? [] : count($Pegawai);
		$data['data'] = $Pegawai == null ? [] : $Pegawai;

		echo json_encode($data);
	}


	public function create_action()
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . validation_errors());
			redirect(site_url('pegawai'));
			//echo validation_errors();
		} else {

			$data_post = $this->input->post();

			if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

				// DO UPLOAD
				$file_name = str_replace('.', '', random_string('alnum', 10));
				$config['upload_path'] = './assets/uploads/';
				$config['allowed_types']        = 'gif|jpg|jpeg|png';
				$config['file_name']            = $file_name;
				$config['overwrite']            = true;
				$config['max_size']             = 1024; // 1MB
				$config['max_width']            = 1080;
				$config['max_height']           = 1080;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('foto')) {
					$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . $this->upload->display_errors());
					redirect(site_url('pegawai'));
				} else {
					$uploaded_data = $this->upload->data();
					$data_post['foto'] = $uploaded_data['file_name'];
				}
			} else {
				$data_post['foto'] = "dummy.jpg";
			}

			$data_pegawai['username'] = random_string('alpha', 5);
			$data_pegawai['password'] = md5("password");
			$data_pegawai['level'] = 3;
			$data_pegawai['created_at'] = date("Y-m-d");

			// Insert Data Pegawai
			$lastest_id = $this->Pegawai_model->insert_user($data_pegawai);

			$data_post['user'] = $lastest_id;

			$this->Pegawai_model->insert_pegawai($data_post);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('pegawai'));
		}
	}

	public function edit($id)
	{

		$row = $this->Pegawai_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_profile' => $row,
				'data_pegawai' => $this->Pegawai_model->get_by_id($id),
				'data_jabatan' => $this->Jabatan_model->get_all(),
				'main_content' => 'pegawai/update',
				'page_title' => 'Edit Pegawai'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('pegawai'));
		}
	}

	public function update_action($id, $type = null)
	{
		if (is_null($type)) {
			$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>');
			redirect(site_url('pegawai'));
		} else {
			if ($type == "profile") {

				$Pegawai = $this->Pegawai_model->get_by_id($id);

				if (empty($Pegawai)) {
					$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan </br>');
					redirect(site_url('pegawai'));
				} else {

					$data_post = $this->input->post();

					if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {

						// DO UPLOAD
						$file_name = str_replace('.', '', random_string('alnum', 10));
						$config['upload_path'] = './assets/uploads/';
						$config['allowed_types']        = 'gif|jpg|jpeg|png';
						$config['file_name']            = $file_name;
						$config['overwrite']            = true;
						$config['max_size']             = 1024; // 1MB
						$config['max_width']            = 1080;
						$config['max_height']           = 1080;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('foto')) {
							$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>' . $this->upload->display_errors());
							redirect(site_url('pegawai'));
						} else {
							$uploaded_data = $this->upload->data();
							$data_post['foto'] = $uploaded_data['file_name'];
						}

					} else {
						$data_post['foto'] = $Pegawai[0]->foto;
					}

					$this->Pegawai_model->update($id, $data_post);
					$this->session->set_flashdata('pesan', 'Data Sukses Diubah</br>');
					redirect(site_url('pegawai/'));
				}
			} elseif ($type == "akun") {
				$Pegawai = $this->Pegawai_model->get_by_id($id);
				$is_username = $this->input->post('username', TRUE) != $Pegawai[0]->username ? '|is_unique[user.username]' : '';
				$this->form_validation->set_rules('username', 'Username', 'required' . $is_username);

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>' . validation_errors());
					redirect(site_url('pegawai'));
				} else {

					if (empty($this->input->post('password'))) {
						$this->Pegawai_model->update_profile($id, ['username' => $this->input->post('username')]);
					} else {
						$this->Pegawai_model->update_profile($id, ['username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))]);
					}

					$this->session->set_flashdata('pesan', 'Data Sukses Diubah');
					redirect(site_url('pegawai'));
				}
			} else {
				$this->session->set_flashdata('pesan', 'Data Gagal Diubah : Method Tidak Diketahui </br>');
				redirect(site_url('pegawai'));
			}
		}

		// $Pegawai = $this->Pegawai_model->get_by_id($id);
		// $is_username = $this->input->post('username', TRUE) != $Pegawai->username ? '|is_unique[user.username]' : '';

		// $this->form_validation->set_rules('username', 'Username', 'required'.$is_username);//|edit_unique[barang.nama.' . $id . ']

		// if ($this->form_validation->run() == FALSE) {
		// 	 $this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>'.validation_errors());
		//     redirect(site_url('pegawai'));
		// } else {
		// 	$data['username'] = $this->input->post('username', TRUE);
		// 	$data['password'] = md5($this->input->post('password', TRUE));

		// 	$this->Pegawai_model->update_profile($id, $data);
		// 	$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
		// 	redirect(site_url('pegawai'));
		// }
	}

	public function delete($id)
	{
		$row = $this->Pegawai_model->get_by_id($id);

		if ($row) {
			$this->Pegawai_model->delete($id);
			$this->Pegawai_model->delete_user($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pegawai'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pegawai'));
		}
	}

	public function print_Pegawai($id = null) // CETAK LAPORAN 
	{
		$data_session = $this->session->userdata;

		if ($id == null) {
			$dataPegawai = $this->Pegawai_model->get_by_idPegawai($data_session['id'])->result();
		} else {
			$dataPegawai = $this->Pegawai_model->get_by_idPegawai($id)->result();
		}


		if ($dataPegawai == null) {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('lineup'));
		} else {
			//var_dump($dataPegawai);
			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "Laporan Pegawai-" . $dataPegawai[0]->name . "-" . $dataPegawai[0]->posisi . ".pdf";
			$this->pdf->load_view('laporan/laporan_Pegawai', ['data' => $dataPegawai]);
		}
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required');
		$this->form_validation->set_rules('nomor_telepon', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('gaji_pokok', 'Gaji Pokok', 'required');
		$this->form_validation->set_rules('tunjangan', 'Tunjangan', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
