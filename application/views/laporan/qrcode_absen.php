<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Generate QR Code Absensi</title>
</head>

<body>

    <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
                <td style="text-align:center;width:20%">
                    <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
                    <p> Komplang Rt 02 / Rw 01, Kadipiro, Banjarsari, Surakarta</p>
                </td>
            </tr>
        </tbody>

    </table>

    <hr>

    <div style="text-align:center">
        <h1>Sesi : <?php echo $data_absen->nama; ?></h1>
        <p style="color: red; font-weight:bold;font-size:1.5em"><?php echo date('H:i',strtotime($data_absen->jam_masuk)) . " S/D " . date('H:i',strtotime($data_absen->jam_pulang)) ." WIB"; ?></p>
        <br>
        <?php
        echo '<img src="' . base_url($img) . '" />';
        ?>
        <p style="font-size: 1.2em;">Masuk, Pulang ataupun Lembur Harap Absen dengan Melakukan Scan Sesuai Sesi Shift Masing-masing Karyawan. Terimakasih.</p>
        <p style="font-weight:bold;">TTD Pimpinan</p>
    </div>
</body>

</html>