<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Ijin Pinrio</title>
</head>

<body>
    <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
                <td style="text-align:center">
                    <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
                    <br>
                    <br>
                    <small>Komplang Rt 02 / Rw 01, Kadipiro, Banjarsari, Surakarta</small>
                    <h4>Laporan Ijin</h4>
                </td>
            </tr>
        </tbody>

    </table>

    <hr>

    <div style="text-align:center">

        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>

                    <th>No.</th>
                    <th>Pegawai</th>
                    <th>Jenis</th>
                    <th>Jumlah Hari</th>
                    <th>Keterangan</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                </tr>

                <?php $counter = 1;
                foreach ($data_ijin as $key) {
                ?>
                    <tr>
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $key->nama_pegawai?></td>
                        <td><?php echo $key->jenis ?></td>
                        <td><?php echo $key->jumlah_hari ?></td>
                        <td><?php echo $key->keterangan ?></td>
                        <td><?php echo $key->tanggal ?></td>
                        <td><?php echo $key->status ?></td>
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>

        <p>&nbsp;</p>

        <p>&nbsp;</p>

        <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td>Yang Mengetahui,</td>
                </tr>
                <tr>
                    <!-- <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td> -->
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Pinrio Merchandise</td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>