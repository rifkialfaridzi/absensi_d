<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Slip Gaji <?php echo $pegawai[0]->nama; ?> Periode : <?php echo $periode->periode; ?></title>
</head>

<body>

    <div style="text-align:center">
        <div><img style="width: auto;height:100px" src="<?php echo base_url('assets/img/logo.png') ?>"></img></div> <br>
        <h2 style="text-align:center"><strong>Slip Gaji Periode : <?php echo $periode->periode; ?></strong></h2>

        <p>&nbsp;</p>

        <table cellpadding="1" cellspacing="0" style="border-top: 1px solid; border-bottom: 1px solid;margin:auto;padding:10px; width:100%;">

            <tbody style="border-spacing: 10px;margin:15px">
                <tr style="border: 0;">
                    <td style="border: 0"><b>Nama</b></td>
                    <td style="border: 0">: <b><?php echo $pegawai[0]->nama; ?></b></td>
                    <td style="border: 0"><b>NIK</b></td>
                    <td style="border: 0">: <b><?php echo $pegawai[0]->nik; ?></b></td>
                </tr>
                <tr style="border: 0;">
                    <td style="border: 0"><b>Jabatan</b></td>
                    <td style="border: 0">: <b><?php echo $pegawai[0]->jabatan; ?></b></td>
                    <td style="border: 0"></td>
                    <td style="border: 0"></td>
                </tr>
            </tbody>
        </table>
        <br>
        <table cellpadding="1" cellspacing="0" style="padding:10px; width:50%;">

            <tbody style="border-spacing: 10px;margin:15px;">
                <tr style="border: 0;">
                    <td style="border: 0">Masuk</td>
                    <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
                    <td style="border: 0;text-align:right"> <?php echo $data_gaji['jumlah_hari_kerja']; ?></td>
                </tr>
                <tr style="border: 0;">
                    <td style="border: 0">Ijin</td>
                    <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
                    <td style="border: 0;text-align:right"> <?php echo $data_gaji['jumlah_ijin']; ?></td>
                </tr>
                <tr style="border: 0;">
                    <td style="border: 0">Cuti</td>
                    <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
                    <td style="border: 0;text-align:right"> <?php echo $data_gaji['jumlah_cuti']; ?></td>
                </tr>
                <tr style="border: 0;">
                    <td style="border: 0">Alpha</td>
                    <td style="border: 0"><b><span style="color:red">(-)</span></b></td>
                    <td style="border: 0;text-align:right"> <?php echo $data_gaji['jumlah_hari_tidakmasuk']; ?></td>
                </tr>
                <tr style="border: 0;">
                    <td style="border: 0">Terlambat</td>
                    <td style="border: 0"><b><span style="color:red">(-)</span></b></td>
                    <td style="border: 0;text-align:right"> <?php echo $data_gaji['waktu_terlambat']; ?> Jam</td>
                </tr>
            </tbody>
        </table>
        <br>
        <table cellpadding="1" cellspacing="0" style="margin:auto;padding:10px; width:100%;">

            <tbody style="border-spacing: 10px;margin:15px;">
                <tr style="border: 0;">
                    <td style="border: 0">Gaji (<?php echo $data_gaji['jumlah_hari_kerja_ijin_cuti']; ?> Absen : <?php echo $periode->jumlah_hari; ?> Hari Kerja) x <?php echo "Rp " . $pegawai[0]->gaji_pokok; ?> (Gaji Pokok)</td>
                    <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
                    <td style="border: 0;text-align:right"> <b><?php echo "Rp " . number_format($data_gaji['gaji'], 0, ",", ","); ?></b></td>
                </tr>

                <tr style="border: 0;">
                    <td style="border: 0">Tunjangan</td>
                    <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
                    <td style="border: 0;text-align:right"> <b><?php echo "Rp " .  $pegawai[0]->tunjangan; ?></b></td>
                </tr>

                <tr style="border: 0;">
                    <td style="border: 0">Lembur (<?php echo $data_gaji['jumlah_jam_lembur']; ?> jam x <?php echo "Rp " . number_format($periode->lembur, 0, ",", ","); ?>)</td>
                    <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
                    <td style="border: 0;text-align:right"> <b><?php echo "Rp " .   number_format($data_gaji['total_bayar_lembur'], 0, ",", ","); ?></b></td>
                </tr>
                <tr style="border: 0;">
                    <td style="border: 0">Total Terlambat (<?php echo $data_gaji['waktu_terlambat']; ?> jam x <?php echo "Rp " . number_format(10000, 0, ",", ","); ?>)</td>
                    <td style="border: 0"><b><span style="color:red">(-)</span></b></td>
                    <td style="border: 0;text-align:right"> <b><?php echo "Rp " .   number_format($data_gaji['potongan_terlambat'], 0, ",", ","); ?></b></td>
                </tr>
            </tbody>
        </table>

        <hr>
        <table cellpadding="1" cellspacing="0" style="margin:auto;padding:10px; width:100%;">

            <tbody style="border-spacing: 10px;margin:15px;">
                <!-- <tr style="border: 0;">
        <td style="border: 0"><b>Gaji Pokok</b></td>
        <td style="border: 0"><b><span style="color:green">(+)</span></b></td>
        <td style="border: 0;text-align:right">: <b><? //php echo "Rp " .  $pegawai[0]->gaji_pokok; 
                                                    ?></b></td>
    </tr> -->

                <tr style="border: 0;">
                    <td style="border: 0"><b>Gaji Yang Diterima</b></td>
                    <td style="border: 0"><b></b></td>
                    <td style="border: 0;text-align:right"> <b><?php $jumlah_gaji = (int)preg_replace("/([^0-9\\.])/i", "", $pegawai[0]->tunjangan)  + intval($data_gaji['total_gaji']);
                                                                echo "Rp " .  number_format($jumlah_gaji, 0, ",", ","); ?></b></td>
                </tr>
            </tbody>
        </table>




        <br>


        <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:90%">
            <tbody>
                <tr>
                    <td>Yang Mengetahui,</td>
                </tr>
                <tr>
                    <td><span style="font-size:16px"><strong>Micga Priyo Pratitis</strong></span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Pimpinan Pinrio Merchandise</td>
                </tr>
            </tbody>
        </table>

        <p>&nbsp;</p>
    </div>

    <p>&nbsp;</p>
</body>

</html>