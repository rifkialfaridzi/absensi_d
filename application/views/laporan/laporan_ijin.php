<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan ijin Pinrio</title>
</head>
<body>
<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

<tbody>
    <tr>
        <td style="text-align:center">
            <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
            <br>
            <br>
            <small>Komplang Rt 02 / Rw 01, Kadipiro, Banjarsari, Surakarta</small>
            <h4>Laporan Gaji Pegawai</h4>
        </td>
    </tr>
</tbody>

</table>

<hr>

<div style="text-align:center">

<p>&nbsp;</p>
<h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">A. Data Pegawai</h4>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>

            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nama<b</td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Jabatan<b</td>
        </tr>

        <?php //foreach ($data as $key) { 
        ?>
        <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pegawai[0]->nama; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pegawai[0]->jabatan; ?></td>
        </tr>
        <?php //} 
        ?>
    </tbody>
</table>

<h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">B. Riwayat Cuti/Ijin</h4>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Jenis</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Jumlah Hari</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Keterangan</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal</b></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Status</b></td>
        </tr>

        <?php foreach ($data_ijin as $key) { 
        ?>
        <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->jenis; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->jumlah_hari; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->keterangan; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->tanggal; ?></td>
            <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key->status; ?></td>
        </tr>
        <?php } 
        ?>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
    <tbody>
        <tr>
            <td>Yang Mengetahui,</td>
        </tr>
        <tr>
            <!-- <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td> -->
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Pinrio Merchandise</td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>