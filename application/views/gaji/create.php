<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Tambah Periode Penggajian</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("gaji/create_action"); ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Periode Gaji</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" name="periode" class="form-control datepickers">
									<div class="invalid-feedback">
										Periode Gaji Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jumlah Hari Kerja</label>
								<div class="col-sm-12 col-md-7">
									<input id="jumlah_hari" type="number" min="1" max="31" class="form-control" name="jumlah_hari" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Jumlah Hari Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bayaran Lembur/Jam</label>
								<div class="col-sm-12 col-md-7">
									<input type="number" class="form-control" name="lembur" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Bayaran Lembur Hari Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-success">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<script>
	document.getElementById("jumlah_hari").addEventListener("change", function() {
		let v = parseInt(this.value);
		if (v < 1) this.value = 1;
		if (v > 50) this.value = 31;
	});

	var cleaveC = new Cleave('.currency', {
		numeral: true,
		numeralThousandsGroupStyle: 'thousand'
	});

	var cleave = new Cleave('.phone-numbers', {
		numericOnly: true,
		delimiters: [' ', ' ', ' '],
		blocks: [4, 4, 4]
	});

	var cleave = new Cleave('.nik-formating', {
		numericOnly: true,
		delimiters: ['.', '.', '.', '.', '.', '-'],
		blocks: [2, 2, 2, 2, 2, 2, 4]
	});
</script>