<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Tambah Shift Baru</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("shift/create_action"); ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Shift</label>
								<div class="col-sm-12 col-md-7">
									<input id="nama" type="text" class="form-control" name="nama" tabindex="1" placeholder="Shift Pagi" required autofocus>
									<div class="invalid-feedback">
										Nama Shift Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jam Masuk</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control timepicker" name="jam_masuk" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Jam Masuk Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jam Keluar</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control timepicker" name="jam_pulang" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Jam Pulang Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-success">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

