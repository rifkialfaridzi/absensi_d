<section ng-app="myApp" ng-controller="someController" class="section">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="card card-statistic-2">
        <div class="card-chart">
          <canvas id="balance-chart" height="80"></canvas>
        </div>
        <div class="card-icon shadow-primary bg-primary">
          <i class="fas fa-users"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Jumlah Pegawai</h4>
          </div>
          <div class="card-body">
            <?php echo count($pegawai); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Terakhir Ditambahkan</h4>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive table-invoice">
            <table class="table table-striped">
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Shift</th>
                <th>Jenis</th>
                <th>Waktu</th>
                <th>Action</th>
              </tr>
              <?php $i = 1;
              foreach ($recent_absen as $key) {

              ?>
                <tr>
                  <td> <?php echo $i; ?></td>
                  <td> <?php echo $key->nama_pegawai; ?></td>
                  <td> <?php echo $key->nama_shift; ?></td>
                  <td> <?php echo $key->jenis; ?></td>
                  <td> <?php echo $key->waktu; ?></td>
                  <td> <?php echo $key->gap > 0 ? "Terlambat" : "Tepat Waktu"; ?></td>
                </tr>

              <?php //if (++$i > 4) break;
              } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  var app = angular.module('myApp', ['ui.bootstrap']);

  app.controller('someController', function($scope, $filter, $http) {



    $scope.selectMonth = function(month) {
      $http.get('<?php echo base_url("admin/dashboard/"); ?>' + month, {
        msg: 'hello word!'
      }).
      then(function(response) {
        $scope.totalSupplier = response.data.supplier;
        $scope.totalOrder = response.data.totalOrder;
        $scope.totalProdukOrder = response.data.totalProdukOrder;
        $scope.totalPendapatan = response.data.totalPendapatan;
        $scope.topProduk = response.data.topProduk;
        $scope.totalProduk = response.data.produk.jumlah_produk;

        console.log(response.data);
      }, function(response) {
        console.log('error bos');
      });

    }

    var date = new Date();
    $scope.selectMonth(date.getMonth() + 1);





  });
</script>