<ul class="navbar-nav">
    <li class="nav-item <?php if ($this->uri->segment(1) == "admin") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("admin"); ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
    <li class="nav-item <?php if ($this->uri->segment(1) == "pegawai") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("pegawai"); ?>"><i class="fas fa-users"></i> <span>Pegawai</span></a></li>
    <li class="nav-item <?php if ($this->uri->segment(1) == "ijin") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("ijin/main"); ?>"><i class="fas fa-users"></i> <span>Perizinan</span></a></li>
    <li class="nav-item <?php if ($this->uri->segment(1) == "gaji") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("gaji/main"); ?>"><i class="fas fa-users"></i> <span>Gaji Pegawai</span></a></li>

    <?php if ($this->session->userdata['username'] == "admin") { ?>
        <li class="nav-item dropdown <?php if ($this->uri->segment(1) == "master") {
                                            echo 'active';
                                        } ?>">
            <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Data Master</span></a>
            <ul class="dropdown-menu">
                <li class="nav-item <?php if ($this->uri->segment(2) == "jabatan") {
                        echo 'active';
                    } ?>"><a class="nav-link" href="<?php echo site_url("/master/jabatan"); ?>">Jabatan</a></li>
                <li class="nav-item <?php if ($this->uri->segment(2) == "shift") {
                        echo 'active';
                    } ?>"><a class="nav-link" href="<?php echo site_url("/master/shift"); ?>">Shift</a></li>
            </ul>
        </li>
    <?php } ?>
</ul>
