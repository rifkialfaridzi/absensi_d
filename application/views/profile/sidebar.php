<ul class="navbar-nav">
    <li class="nav-item <?php if ($this->uri->segment(1) == "admin") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("profile"); ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>

    <li class="nav-item dropdown <?php if ($this->uri->segment(2) == "absen") {
                                        echo 'active';
                                    } ?>">
        <a href="<?php echo site_url("profile/absen"); ?>" data-toggle="dropdown" class="nav-link has-dropdown"><i class="fas fa-columns"></i><span>Absen</span></a>
        <ul class="dropdown-menu">
            <li class="nav-item <?php if ($this->uri->segment(3) == "masuk") {
                                    echo 'active';
                                } ?>"><a class="nav-link" href="<?php echo site_url("profile/absen/masuk"); ?>">Absen Masuk</a></li>
            <li class="nav-item <?php if ($this->uri->segment(3) == "pulang") {
                                    echo 'active';
                                } ?>"><a class="nav-link" href="<?php echo site_url("profile/absen/pulang"); ?>">Absen Pulang</a></li>

        </ul>
    </li>

    <li class="nav-item dropdown <?php if ($this->uri->segment(2) == "lembur") {
                                        echo 'active';
                                    } ?>">
        <a href="<?php echo site_url("profile/lembur"); ?>" data-toggle="dropdown" class="nav-link has-dropdown"><i class="fas fa-columns"></i><span>Lembur</span></a>
        <ul class="dropdown-menu">
            <li class="nav-item <?php if ($this->uri->segment(3) == "masuk") {
                                    echo 'active';
                                } ?>"><a class="nav-link" href="<?php echo site_url("profile/lembur/masuk"); ?>">Lembur Masuk</a></li>
            <li class="nav-item <?php if ($this->uri->segment(3) == "pulang") {
                                    echo 'active';
                                } ?>"><a class="nav-link" href="<?php echo site_url("profile/lembur/pulang"); ?>">Lembur Pulang</a></li>
        </ul>
    </li>
    <li class="nav-item <?php if ($this->uri->segment(1) == "ijin") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("ijin"); ?>"><i class="fas fa-sticky-note"></i> <span>Izin</span></a></li>
     <li class="nav-item <?php if ($this->uri->segment(1) == "gaji") {
                            echo 'active';
                        } ?>"><a class="nav-link" href="<?php echo site_url("gaji/periode"); ?>"><i class="fas fa-money-bill"></i> <span>Gaji</span></a></li>
</ul>
