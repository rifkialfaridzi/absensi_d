<section class="section">
	<div class="section-header">
		<div class="section-header-back">
			<button onclick="history.back()" class="btn btn-icon"><i class="fas fa-arrow-left"></i></button>
		</div>
		<h1>Tambah Perijinan</h1>
	</div>

	<div class="section-body">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4>Buat Baru</h4>
					</div>
					<div class="card-body">

						<form method="POST" action="<?php echo base_url("ijin/update_action/").$data_ijin->id; ?>" class="needs-validation" novalidate="">
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keperluan</label>
								<div class="col-sm-12 col-md-7">
									<select name="jenis" class="form-control selectric">
										<option <?php echo $data_ijin->jenis == "cuti" ? "selected" :""; ?> value="cuti">Cuti</option>
										<option <?php echo $data_ijin->jenis == "ijin" ? "selected" :""; ?> value="ijin">Ijin</option>
									</select>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jumlah Hari</label>
								<div class="col-sm-12 col-md-7">
									<input type="number" class="form-control" name="jumlah_hari" value="<?php echo $data_ijin->jumlah_hari ?>" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Jumlah Hari Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
								<div class="col-sm-12 col-md-7">
									<input type="text" class="form-control" value="<?php echo $data_ijin->keterangan ?>" name="keterangan" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Keterangan Masih Kosong
									</div>
								</div>
							</div>
							<div class="form-group row mb-4">
								<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
								<div class="col-sm-12 col-md-7">
									<button class="btn btn-primary">Update</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ADDONS LIBRARY -->
<script src="<?php echo base_url('assets/modules/cleave-js/dist/cleave.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/modules/cleave-js/dist/addons/cleave-phone.au.js'); ?>"></script>

<script>
	var cleaveC = new Cleave('.currency', {
		numeral: true,
		numeralThousandsGroupStyle: 'thousand'
	});

	var cleave = new Cleave('.phone-numbers', {
		numericOnly: true,
		delimiters: [' ', ' ', ' '],
		blocks: [4, 4, 4]
	});

	var cleave = new Cleave('.nik-formating', {
		numericOnly: true,
		delimiters: ['.', '.', '.', '.', '.', '-'],
		blocks: [2, 2, 2, 2, 2, 2, 4]
	});
</script>