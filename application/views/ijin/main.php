<section class="section">
	<div class="section-header">
		<h1>Halaman Perijinan</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Perijinan</h4>
						<div class="card-header-action">
							<a id="cetak" target="_blank" href="<?php echo base_url('ijin/print_ijin/'); ?>" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="tabel_ijin" class="table table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Pegawai</th>
										<th>Jenis</th>
										<th>Jumlah Hari</th>
										<th>Keterangan</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Catatan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Perijinan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		counter = 1;
		table = $('#tabel_ijin').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('ijin/main_data'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						return counter++;
					}
				},
				{
					"data": "nama_pegawai"
				},
				{
					"data": "jenis"
				},
				{
					"data": "jumlah_hari"
				},
				{
					"data": "keterangan"
				},
				{
					"data": "tanggal"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						
						if (row.status == 'diproses') {
							return "<div class='badge badge-info'>Perlu Di Tanggapi</div>";
						}else if(row.status == 'diterima'){
							return "<div class='badge badge-success'>Diterima</div>";
						}else{
							return "<div class='badge badge-danger'>Ditolak</div>"
						}
						
					}
					
				},
				{
					"data": null,
					"render": function(data, type, row) {
						
						if (row.status_keterangan == "") {
							return "tidak ada catatan";
						}else{
							return row.status_keterangan;
						}
						
					}
					
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if ("<?php echo $this->session->userdata['username'];?>" == "admin") {
							return '<a href="<?php echo site_url("ijin/detail/") ?>' + row.id + '" class="btn btn-icon btn-success"><i class="fas fa-info-circle"></i></a>'
							// return '<div class="dropdown d-inline"><button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pilihan</button><div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 29px, 0px);"><a class="dropdown-item has-icon" href="<?php //echo site_url("ijin/update_status/diterima/") ?>' + row.id + '"><i class="fas fa-check-circle"></i>Diterima</a><a class="dropdown-item has-icon" href="<?php //echo site_url("ijin/update_status/ditolak/") ?>' + row.id + '"><i class="fas fa-times-circle"></i> Ditolak</a></div></div>';
						}else{
							return '-';
						}
						
					}
					
				}
			],

		});



	});

	function confirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("ijin/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>