<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <link rel="icon" type="image/png" href="<?php echo base_url('assets\teleo.png') ?>" />
  <title><?php echo $page_title; ?></title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

  <!-- CSS Libraries -->
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap-datetimepicker.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap-daterangepicker/daterangepicker.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/datatables.min.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/select2/dist/css/select2.min.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/jquery-selectric/selectric.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/modules/summernote/summernote-bs4.css"); ?>>


  <!-- Template CSS -->
  <link rel="stylesheet" href=<?php echo base_url("assets/css/style.css"); ?>>
  <link rel="stylesheet" href=<?php echo base_url("assets/css/components.css"); ?>>
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script src=<?php echo base_url("assets/modules/jquery.min.js"); ?>></script>

  <!-- JS Libraies -->
  <script src=<?php echo base_url("assets/modules/datatables/datatables.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/jquery-ui/jquery-ui.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js"); ?>></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
  <script src=<?php echo base_url("assets/js/chart.js"); ?>></script>
  <script src=<?php echo base_url("assets/js/qrscanner.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/summernote/summernote-bs4.js"); ?>></script>


  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->
</head>

<body class="layout-3" background="<?php echo base_url('assets/img/b13.jpg'); ?>">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <a href="#" class="navbar-brand sidebar-gone-hide">Absensi dan Penggajian</a>
        <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
        <div class="nav-collapse">
          <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
          </a>
        </div>
        <form class="form-inline ml-auto">
          <ul class="navbar-nav">
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>

        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <img alt="image" src="<?php echo base_url('assets/img/avatar/avatar-1.png') ?>" class="rounded-circle mr-1">
              <div class="d-sm-none d-lg-inline-block">Halo, <?php echo $this->session->userdata['username']; ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-divider"></div>
              <a href="<?php echo site_url("auth/logout"); ?>" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>

      <nav class="navbar navbar-secondary navbar-expand-lg">
        <div class="container">
          <?php

          $data_session = $this->session->userdata;

          switch ($data_session['level']) {
            case "1":
              $sidebar = 'admin/sidebar';
              break;
            case "2":
              $sidebar = 'admin/sidebar_lev_2';
              break;
            case "3":
              $sidebar = 'profile/sidebar';
              break;
          }



          $this->load->view($sidebar);


          ?>
        </div>
      </nav>


      <!-- Main Content -->
      <div class="main-content">
        <?php $this->load->view($main_content); ?>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2022 <div class="bullet"></div> <a href="#">Sistem Absensi dan Penggajian</a>
        </div>
        <div class="footer-right">

        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->

  <script src=<?php echo base_url("assets/modules/popper.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/tooltip.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/bootstrap/js/bootstrap.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/nicescroll/jquery.nicescroll.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/moment.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/js/stisla.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/bootstrap-daterangepicker/daterangepicker.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/bootstrap-datetimepicker.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/select2/dist/js/select2.full.min.js"); ?>></script>
  <script src=<?php echo base_url("assets/modules/jquery-selectric/jquery.selectric.min.js"); ?>></script>




  <!-- Page Specific JS File -->

  <!-- Template JS File -->

  <script src=<?php echo base_url("assets/js/scripts.js"); ?>></script>
  <script src=<?php echo base_url("assets/js/custom.js"); ?>></script>
  <script>
    $('.datepickers').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD'
      },
      singleDatePicker: true,
    });
  </script>

</body>

</html>