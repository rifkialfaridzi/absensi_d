<section class="section">
	<div class="section-header">
		<h1>Halaman Pegawai</h1>
	</div>

	<div class="section-body">

		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan'); ?>
				</div>
			</div>
		<?php } ?>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Pegawai</h4>
						<?php if ($this->session->userdata['username'] == "admin") { ?>
						<div class="card-header-action">
							<a href="<?php echo base_url('/pegawai/create'); ?>" class="btn btn-success">
							<i class="fa fa-plus"></i> Tambah
							</a>
						</div>
						<?php }?>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="category_tabels" class="table table-striped">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Nomor Telepon</th>
										<th>NIK</th>
										<th>Alamat</th>
										<th>Tanggal Masuk</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Pegawai</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#category_tabels').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pegawai/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("pegawai/detail/") ?>' + row.user + '">' + row.nama + '</a>';
					}
				},
				{
					"data": "jabatan"
				},
				{
					"data": "nomor_telepon"
				},
				{
					"data": "nik"
				},
				{
					"data": "alamat"
				},
				{
					"data": "tanggal_masuk"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if ("<?php echo $this->session->userdata['username'];?>" == "admin") {
							return '<a href="<?php echo site_url("pegawai/edit/") ?>' + row.user + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.user + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
						}else{
							return "-";
						}
						
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("pegawai/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>